/*
 Navicat Premium Data Transfer

 Source Server         : Proyecto
 Source Server Type    : MySQL
 Source Server Version : 50619
 Source Host           : 10.37.129.3
 Source Database       : proyecto

 Target Server Type    : MySQL
 Target Server Version : 50619
 File Encoding         : utf-8

 Date: 03/27/2015 18:35:46 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `admins`
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `idadmin` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) NOT NULL,
  `password` varchar(64) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `correo` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`idadmin`),
  UNIQUE KEY `uniq_admin` (`usuario`,`correo`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `admins`
-- ----------------------------
BEGIN;
INSERT INTO `admins` VALUES ('1', 'admin', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', 'Administrador', 'Sistema', 'jorge.fernandez@kapa7.com'), ('2', 'oscar', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'Oscar', 'Torres', 'oscartorres10@unitec.edu'), ('3', 'ritchie', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'Ritchie', 'Funes', 'ritchfunes@unitec.edu'), ('4', 'jorge', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'Jorge', 'Fernandez', 'jorgefer_00@unitec.edu');
COMMIT;

-- ----------------------------
--  Table structure for `calendario`
-- ----------------------------
DROP TABLE IF EXISTS `calendario`;
CREATE TABLE `calendario` (
  `idCalendario` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `trabajoDesde` date NOT NULL,
  `trabajoHasta` date NOT NULL,
  `lock` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`idCalendario`),
  UNIQUE KEY `indexFechasUnicas` (`trabajoDesde`,`trabajoHasta`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `calendario`
-- ----------------------------
BEGIN;
INSERT INTO `calendario` VALUES ('1', '2015-03-22', '2015-03-28', b'0'), ('16', '2015-03-29', '2015-04-04', b'0'), ('17', '2015-03-15', '2015-03-21', b'0'), ('18', '2015-03-08', '2015-03-14', b'0'), ('19', '2015-03-01', '2015-03-07', b'0');
COMMIT;

-- ----------------------------
--  Table structure for `calendariodetail`
-- ----------------------------
DROP TABLE IF EXISTS `calendariodetail`;
CREATE TABLE `calendariodetail` (
  `idCalendarioDetail` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idCalendario` int(11) unsigned NOT NULL,
  `idEmpleado` int(11) unsigned NOT NULL,
  `codigoLaborLunes` char(5) DEFAULT NULL,
  `codigoLaborMartes` char(5) DEFAULT NULL,
  `codigoLaborMiercoles` char(5) DEFAULT NULL,
  `codigoLaborJueves` char(5) DEFAULT NULL,
  `codigoLaborViernes` char(5) DEFAULT NULL,
  `codigoLaborSabado` char(5) DEFAULT NULL,
  `codigoLaborDomingo` char(5) DEFAULT NULL,
  PRIMARY KEY (`idCalendarioDetail`,`idCalendario`),
  UNIQUE KEY `indexCalEmple` (`idCalendario`,`idEmpleado`) USING BTREE,
  KEY `codigoLaborMartes` (`codigoLaborMartes`) USING BTREE,
  KEY `codigoLaborMiercoles` (`codigoLaborMiercoles`) USING BTREE,
  KEY `codigoLaborJueves` (`codigoLaborJueves`) USING BTREE,
  KEY `codigoLaborViernes` (`codigoLaborViernes`) USING BTREE,
  KEY `codigoLaborSabado` (`codigoLaborSabado`) USING BTREE,
  KEY `codigoLaborDomingo` (`codigoLaborDomingo`) USING BTREE,
  KEY `codigoLaborLunes` (`codigoLaborLunes`) USING BTREE,
  KEY `fkEmple` (`idEmpleado`),
  CONSTRAINT `fkCal` FOREIGN KEY (`idCalendario`) REFERENCES `calendario` (`idCalendario`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fkCodD` FOREIGN KEY (`codigoLaborDomingo`) REFERENCES `labores` (`codigoLabor`) ON UPDATE CASCADE,
  CONSTRAINT `fkCodJ` FOREIGN KEY (`codigoLaborJueves`) REFERENCES `labores` (`codigoLabor`) ON UPDATE CASCADE,
  CONSTRAINT `fkCodL` FOREIGN KEY (`codigoLaborLunes`) REFERENCES `labores` (`codigoLabor`) ON UPDATE CASCADE,
  CONSTRAINT `fkCodM` FOREIGN KEY (`codigoLaborMartes`) REFERENCES `labores` (`codigoLabor`) ON UPDATE CASCADE,
  CONSTRAINT `fkCodMi` FOREIGN KEY (`codigoLaborMiercoles`) REFERENCES `labores` (`codigoLabor`) ON UPDATE CASCADE,
  CONSTRAINT `fkCodS` FOREIGN KEY (`codigoLaborSabado`) REFERENCES `labores` (`codigoLabor`) ON UPDATE CASCADE,
  CONSTRAINT `fkCodV` FOREIGN KEY (`codigoLaborViernes`) REFERENCES `labores` (`codigoLabor`) ON UPDATE CASCADE,
  CONSTRAINT `fkEmple` FOREIGN KEY (`idEmpleado`) REFERENCES `empleados` (`idEmpleado`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `calendariodetail`
-- ----------------------------
BEGIN;
INSERT INTO `calendariodetail` VALUES ('2', '1', '2', 'JEFEO', 'ELECT', 'PINPO', 'PINTP', 'JEFEO', null, 'ELECT'), ('10', '1', '9', 'CERAM', 'JEFEO', 'ELECT', 'FONT', 'PINTP', null, 'ELECT'), ('25', '16', '2', null, null, null, null, null, null, null), ('26', '16', '9', null, null, null, null, null, null, null), ('27', '17', '2', null, null, null, null, null, null, null), ('28', '17', '9', null, null, null, null, null, null, null), ('29', '18', '2', null, null, null, null, null, null, null), ('30', '18', '9', null, null, null, null, null, null, null), ('31', '19', '2', null, null, null, null, null, null, null), ('32', '19', '9', null, null, null, null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `deducciones`
-- ----------------------------
DROP TABLE IF EXISTS `deducciones`;
CREATE TABLE `deducciones` (
  `codDeduccion` char(4) NOT NULL,
  `tipoDeduccion` bit(1) NOT NULL,
  `valorDeduccion` double NOT NULL,
  PRIMARY KEY (`codDeduccion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `deducciones`
-- ----------------------------
BEGIN;
INSERT INTO `deducciones` VALUES ('CHAM', b'0', '100'), ('ISR', b'1', '10');
COMMIT;

-- ----------------------------
--  Table structure for `empleados`
-- ----------------------------
DROP TABLE IF EXISTS `empleados`;
CREATE TABLE `empleados` (
  `idEmpleado` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombreEmpleado` varchar(30) NOT NULL,
  `apellidoEmpleado` varchar(30) NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `numeroIdentidad` char(13) NOT NULL,
  `numeroTelefonico` char(8) NOT NULL,
  `cuentaBancaria` char(20) DEFAULT NULL,
  PRIMARY KEY (`idEmpleado`),
  UNIQUE KEY `index_numeroidentidad` (`numeroIdentidad`),
  UNIQUE KEY `index_telefono` (`numeroTelefonico`),
  UNIQUE KEY `index_cuentabancaria` (`cuentaBancaria`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `empleados`
-- ----------------------------
BEGIN;
INSERT INTO `empleados` VALUES ('2', 'Oscar', 'Torres', '1990-01-01', '0501199001234', '12345567', '21-123-34534-1231231'), ('9', 'Richie', 'Funez', '1990-01-01', '0501199001235', '12345568', '21-123-34534-12312');
COMMIT;

-- ----------------------------
--  Table structure for `labores`
-- ----------------------------
DROP TABLE IF EXISTS `labores`;
CREATE TABLE `labores` (
  `codigoLabor` char(5) NOT NULL,
  `nombreLabor` varchar(20) NOT NULL,
  `unidadMedida` char(4) NOT NULL,
  `pagoPorUnidad` double NOT NULL,
  PRIMARY KEY (`codigoLabor`),
  UNIQUE KEY `nombreLabor` (`nombreLabor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `labores`
-- ----------------------------
BEGIN;
INSERT INTO `labores` VALUES ('CERAM', 'Ceramica', 'MTS2', '30'), ('ELECT', 'Electricidad', 'HR', '120'), ('FONT', 'Fontaneria', 'HR', '85'), ('JEFEO', 'Jefe de Obra', 'HR', '130'), ('PBLQ', 'Pegue de Bloques', 'MTS2', '10'), ('PINPO', 'Pintar Porton', 'MTS', '6'), ('PINTP', 'Pintar Paredes', 'MTS2', '5'), ('RED', 'Redes', 'PIES', '40'), ('REFR', 'Refrigeracion', 'HR', '100'), ('SOLDA', 'Soldar', 'UD', '50'), ('TBYES', 'Tabla Yeso', 'MTS2', '60'), ('TECHO', 'Techo', 'LAM', '60');
COMMIT;

-- ----------------------------
--  Table structure for `planilla`
-- ----------------------------
DROP TABLE IF EXISTS `planilla`;
CREATE TABLE `planilla` (
  `idPlanilla` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idCalendario` int(11) unsigned NOT NULL,
  `subtotalPlanilla` double unsigned NOT NULL DEFAULT '0',
  `totalDeducciones` double unsigned NOT NULL DEFAULT '0',
  `totalPlanilla` double unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idPlanilla`,`idCalendario`),
  UNIQUE KEY `idCalendario` (`idCalendario`),
  CONSTRAINT `fkCalendario` FOREIGN KEY (`idCalendario`) REFERENCES `calendario` (`idCalendario`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `planilla`
-- ----------------------------
BEGIN;
INSERT INTO `planilla` VALUES ('1', '1', '0', '0', '0'), ('6', '16', '0', '0', '0'), ('7', '19', '0', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `planilladetail`
-- ----------------------------
DROP TABLE IF EXISTS `planilladetail`;
CREATE TABLE `planilladetail` (
  `idPlanillaDetail` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idPlanilla` int(11) unsigned NOT NULL,
  `idCalendarioDetail` int(11) unsigned NOT NULL,
  `cantidadLaborLunes` double unsigned DEFAULT '0',
  `cantidadLaborMartes` double unsigned DEFAULT '0',
  `cantidadLaborMiercoles` double unsigned DEFAULT '0',
  `cantidadLaborJueves` double unsigned DEFAULT '0',
  `cantidadLaborViernes` double unsigned DEFAULT '0',
  `cantidadLaborSabado` double unsigned DEFAULT '0',
  `cantidadLaborDomingo` double unsigned DEFAULT '0',
  `deduccionesAEmpleado` double unsigned DEFAULT '0',
  `totalAPagarEmpleado` double unsigned DEFAULT '0',
  PRIMARY KEY (`idPlanillaDetail`,`idPlanilla`),
  UNIQUE KEY `idCalendarioDetail` (`idCalendarioDetail`) USING BTREE,
  KEY `fkPlanillaId` (`idPlanilla`),
  CONSTRAINT `fkCalDetail` FOREIGN KEY (`idCalendarioDetail`) REFERENCES `calendariodetail` (`idCalendarioDetail`) ON UPDATE CASCADE,
  CONSTRAINT `fkPlanillaId` FOREIGN KEY (`idPlanilla`) REFERENCES `planilla` (`idPlanilla`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `planilladetail`
-- ----------------------------
BEGIN;
INSERT INTO `planilladetail` VALUES ('1', '1', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0'), ('2', '1', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0'), ('12', '7', '31', '0', '0', '0', '0', '0', '0', '0', '0', '0'), ('13', '7', '32', '0', '0', '0', '0', '0', '0', '0', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `planilladetaildeduccion`
-- ----------------------------
DROP TABLE IF EXISTS `planilladetaildeduccion`;
CREATE TABLE `planilladetaildeduccion` (
  `idPlanillaDetail` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idPlanillaDetail`),
  CONSTRAINT `fkPlanillaDetail` FOREIGN KEY (`idPlanillaDetail`) REFERENCES `planilladetail` (`idPlanillaDetail`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Procedure structure for `crear_calendario_semanal`
-- ----------------------------
DROP PROCEDURE IF EXISTS `crear_calendario_semanal`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `crear_calendario_semanal`(IN idCalendario int)
BEGIN
	DECLARE n int default 0;
	DECLARE i int default 0;
	DECLARE idEm int default 0;
	SELECT COUNT(*) FROM empleados INTO n;
	WHILE i<n DO
		SELECT idEmpleado FROM empleados LIMIT i,1 INTO idEm;
		CALL crear_schedule(idCalendario, idEm);
		SET i = i+1;
	END WHILE;
END
 ;;
delimiter ;

-- ----------------------------
--  Procedure structure for `crear_planilla`
-- ----------------------------
DROP PROCEDURE IF EXISTS `crear_planilla`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `crear_planilla`(IN idCalendarioInput int(11))
BEGIN
	DECLARE id int default 0;
	DECLARE n int default 0;
	DECLARE i int default 0;
	DECLARE idCalD int default 0;
	INSERT into planilla(idCalendario) VALUES (idCalendarioInput);
	SELECT LAST_INSERT_ID() INTO id;
	SELECT COUNT(*) FROM calendariodetail INTO n;
	WHILE i<n DO
		IF(EXISTS(SELECT idCalendarioDetail FROM calendariodetail WHERE idCalendario=idCalendarioInput LIMIT i,1)) then
			SELECT idCalendarioDetail FROM calendariodetail WHERE idCalendario=idCalendarioInput LIMIT i,1 into idCalD; 
			CALL crear_planilla_detail(id, idCalD);
		END IF;
		SET i = i+1;
	END WHILE;
END
 ;;
delimiter ;

-- ----------------------------
--  Procedure structure for `crear_planilla_detail`
-- ----------------------------
DROP PROCEDURE IF EXISTS `crear_planilla_detail`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `crear_planilla_detail`(IN idPlanilla int(11), IN idCalendarioDetail int(11))
BEGIN
	START TRANSACTION;
		INSERT INTO planilladetail(idPlanilla, idCalendarioDetail) VALUES (idPlanilla, idCalendarioDetail);
		COMMIT;
END
 ;;
delimiter ;

-- ----------------------------
--  Procedure structure for `crear_schedule`
-- ----------------------------
DROP PROCEDURE IF EXISTS `crear_schedule`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `crear_schedule`(IN idCalendario int(11), IN idEmpleado int(11))
BEGIN
	START TRANSACTION;
		INSERT INTO calendariodetail(idCalendario, idEmpleado) VALUES (idCalendario, idEmpleado);
		COMMIT;
END
 ;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
