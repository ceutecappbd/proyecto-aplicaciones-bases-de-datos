﻿namespace Proyecto_Aplicaciones_BD.Administrar
{
    partial class FormAddEditDeduccion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.codigoTextbox = new System.Windows.Forms.TextBox();
            this.valorTextbox = new System.Windows.Forms.TextBox();
            this.tipoDeduccion = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tipoDeduccion);
            this.groupBox1.Controls.Add(this.valorTextbox);
            this.groupBox1.Controls.Add(this.codigoTextbox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(268, 207);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Deduccion";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(125, 242);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Location = new System.Drawing.Point(206, 242);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 2;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Codigo de Deduccion :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tipo de Deduccion :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Valor de Deduccion (Fijo o %) :";
            // 
            // codigoTextbox
            // 
            this.codigoTextbox.Location = new System.Drawing.Point(165, 47);
            this.codigoTextbox.MaxLength = 4;
            this.codigoTextbox.Name = "codigoTextbox";
            this.codigoTextbox.Size = new System.Drawing.Size(97, 20);
            this.codigoTextbox.TabIndex = 3;
            // 
            // valorTextbox
            // 
            this.valorTextbox.Location = new System.Drawing.Point(165, 115);
            this.valorTextbox.MaxLength = 20;
            this.valorTextbox.Name = "valorTextbox";
            this.valorTextbox.Size = new System.Drawing.Size(97, 20);
            this.valorTextbox.TabIndex = 5;
            // 
            // tipoDeduccion
            // 
            this.tipoDeduccion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tipoDeduccion.FormattingEnabled = true;
            this.tipoDeduccion.Items.AddRange(new object[] {
            "Fijo",
            "Porcentaje"});
            this.tipoDeduccion.Location = new System.Drawing.Point(165, 79);
            this.tipoDeduccion.Name = "tipoDeduccion";
            this.tipoDeduccion.Size = new System.Drawing.Size(97, 21);
            this.tipoDeduccion.TabIndex = 6;
            this.tipoDeduccion.SelectedIndexChanged += new System.EventHandler(this.tipoDeduccion_SelectedIndexChanged);
            // 
            // FormAddEditDeduccion
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(293, 277);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormAddEditDeduccion";
            this.Text = "Agregar/Editar Deduccion";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.ComboBox tipoDeduccion;
        private System.Windows.Forms.TextBox valorTextbox;
        private System.Windows.Forms.TextBox codigoTextbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;

    }
}