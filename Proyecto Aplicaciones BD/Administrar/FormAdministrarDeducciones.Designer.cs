﻿namespace Proyecto_Aplicaciones_BD.Administrar
{
    partial class FormAdministrarDeducciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewDeducciones = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.codDeduccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoDeduccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorDeduccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDeducciones)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewDeducciones
            // 
            this.dataGridViewDeducciones.AllowUserToAddRows = false;
            this.dataGridViewDeducciones.AllowUserToDeleteRows = false;
            this.dataGridViewDeducciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDeducciones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codDeduccion,
            this.tipoDeduccion,
            this.valorDeduccion});
            this.dataGridViewDeducciones.Location = new System.Drawing.Point(6, 19);
            this.dataGridViewDeducciones.Name = "dataGridViewDeducciones";
            this.dataGridViewDeducciones.ReadOnly = true;
            this.dataGridViewDeducciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewDeducciones.Size = new System.Drawing.Size(457, 298);
            this.dataGridViewDeducciones.TabIndex = 0;
            this.dataGridViewDeducciones.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewDeducciones_CellFormatting);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewDeducciones);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(482, 323);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Deducciones";
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(501, 32);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(119, 23);
            this.btnAgregar.TabIndex = 2;
            this.btnAgregar.Text = "Agregar Deduccion";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(501, 61);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(119, 23);
            this.btnEditar.TabIndex = 3;
            this.btnEditar.Text = "Editar Deduccion";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnBorrar
            // 
            this.btnBorrar.Location = new System.Drawing.Point(501, 90);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(119, 23);
            this.btnBorrar.TabIndex = 4;
            this.btnBorrar.Text = "Borrar Deduccion";
            this.btnBorrar.UseVisualStyleBackColor = true;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // codDeduccion
            // 
            this.codDeduccion.DataPropertyName = "codDeduccion";
            this.codDeduccion.HeaderText = "Codigo de Deduccion";
            this.codDeduccion.Name = "codDeduccion";
            this.codDeduccion.ReadOnly = true;
            this.codDeduccion.Width = 140;
            // 
            // tipoDeduccion
            // 
            this.tipoDeduccion.DataPropertyName = "tipoDeduccion";
            this.tipoDeduccion.HeaderText = "Tipo de Deduccion (0 Fijo, 1 Porcentaje)";
            this.tipoDeduccion.Name = "tipoDeduccion";
            this.tipoDeduccion.ReadOnly = true;
            this.tipoDeduccion.Width = 130;
            // 
            // valorDeduccion
            // 
            this.valorDeduccion.DataPropertyName = "valorDeduccion";
            this.valorDeduccion.HeaderText = "Valor de Deduccion";
            this.valorDeduccion.Name = "valorDeduccion";
            this.valorDeduccion.ReadOnly = true;
            this.valorDeduccion.Width = 130;
            // 
            // FormAdministrarDeducciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 347);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "FormAdministrarDeducciones";
            this.Text = "Administrar Deducciones";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDeducciones)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewDeducciones;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.DataGridViewTextBoxColumn codDeduccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoDeduccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorDeduccion;
    }
}