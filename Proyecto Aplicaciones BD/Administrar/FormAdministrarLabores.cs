﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Proyecto_Aplicaciones_BD.Administrar
{
    public partial class FormAdministrarLabores : Form
    {

        private MySqlConnection Conexion_MySQL;
        public FormAdministrarLabores(FormaPrincipal parent, MySqlConnection con)
        {
            InitializeComponent();
            this.MaximumSize = this.MinimumSize = this.Size;
            Conexion_MySQL = con;
            this.MdiParent = parent;

            ActualizarTabla();
        }


        private void btnAgregarLabor_Click(object sender, EventArgs e)
        {
            FormAddEditLabor form = new FormAddEditLabor(Conexion_MySQL);
            form.ShowDialog();
            ActualizarTabla();
        }

        private void btnEditarLabor_Click(object sender, EventArgs e)
        {
            if(dataGridViewLabores.SelectedRows.Count>0)
            {
                FormAddEditLabor form = new FormAddEditLabor(Conexion_MySQL, dataGridViewLabores.SelectedRows[0].Cells[0].Value.ToString());
                form.ShowDialog();
                ActualizarTabla();
            }
            else
            {
                MessageBox.Show("Debe seleccionar una labor primero");
            }
        }

        private void btnBorrarLabor_Click(object sender, EventArgs e)
        {
            if (dataGridViewLabores.SelectedRows.Count > 0)
            {
                try
                {
                    MySqlCommand query = new MySqlCommand("DELETE FROM labores WHERE codigoLabor=@codigolabor", Conexion_MySQL);
                    query.Parameters.AddWithValue("@codigolabor", dataGridViewLabores.SelectedRows[0].Cells[0].Value.ToString());

                    DialogResult resultado = MessageBox.Show("Desea borrar esta labor?", "Borrar labor", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);

                    if (resultado == DialogResult.Yes)
                    {
                        if (query.ExecuteNonQuery() == 1)
                        {
                            MessageBox.Show("Labor borrada exitosamente");
                            ActualizarTabla();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ocurrio un error al borrar la labor. Mensaje : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                ActualizarTabla();
            }
            else
            {
                MessageBox.Show("Debe seleccionar una labor primero");
            }
        }

        private void ActualizarTabla()
        {
            try
            {

                DataTable table = new DataTable();
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter("SELECT * from labores", Conexion_MySQL);

                dataAdapter.Fill(table);

                dataGridViewLabores.DataSource = table;

            }
            catch (Exception e)
            {
                MessageBox.Show("Error al actualizar tabla de labores. Mensaje : " + e.Message, "Error al actualizar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


    }
}
