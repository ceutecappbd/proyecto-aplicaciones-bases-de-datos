﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Proyecto_Aplicaciones_BD.Administrar
{
    public partial class FormAdministrarEmpleados : Form
    {

        private MySqlConnection Conexion_MySQL;

        public FormAdministrarEmpleados(FormaPrincipal parent, MySqlConnection con)
        {
            InitializeComponent();
            this.MaximumSize = this.MinimumSize = this.Size;
            Conexion_MySQL = con;
            this.MdiParent = parent;

            ActualizarTabla();
        }

        private void btnAgregarEmpleado_Click(object sender, EventArgs e)
        {
            FormAddEditEmpleado addEmployee = new FormAddEditEmpleado(Conexion_MySQL);
            addEmployee.ShowDialog();
            ActualizarTabla();
        }

        private void btnEditarEmpleado_Click(object sender, EventArgs e)
        {
            if (dataGridViewEmpleados.SelectedRows.Count > 0)
            {
                FormAddEditEmpleado editEmployee = new FormAddEditEmpleado(Conexion_MySQL, int.Parse(dataGridViewEmpleados.SelectedRows[0].Cells[0].Value.ToString()));
                editEmployee.ShowDialog();
                ActualizarTabla();
            }
            else
            {
                MessageBox.Show("Debe seleccionar un empleado primero");
            }
        }

        private void btnBorrarEmpleado_Click(object sender, EventArgs e)
        {
            if(dataGridViewEmpleados.SelectedRows.Count>0)
            {
                try
                {
                    MySqlCommand query = new MySqlCommand("DELETE FROM empleados WHERE idEmpleado=@idempleado", Conexion_MySQL);
                    query.Parameters.AddWithValue("@idempleado", int.Parse(dataGridViewEmpleados.SelectedRows[0].Cells[0].Value.ToString()));

                    DialogResult resultado = MessageBox.Show("Desea borrar al empleado?", "Borrar empleado", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);

                    if(resultado == DialogResult.Yes)
                    {
                        if(query.ExecuteNonQuery()==1)
                        {
                            MessageBox.Show("Empleado borrado exitosamente");
                            ActualizarTabla();
                        }
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Ocurrio un error al borrar el empleado. Mensaje : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                ActualizarTabla();
            }
            else
            {
                MessageBox.Show("Debe seleccionar un empleado primero");
            }
        }

        private void ActualizarTabla()
        {

            pictureBox1.Visible = true;

            try
            {

                DataTable table = new DataTable();
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter("SELECT * from empleados", Conexion_MySQL);

                dataAdapter.Fill(table);

                dataGridViewEmpleados.DataSource = table;

            }
            catch(Exception e)
            {
                MessageBox.Show("Error al actualizar tabla de empleados. Mensaje : " + e.Message, "Error al actualizar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            pictureBox1.Visible = false;
        }
    }
}
