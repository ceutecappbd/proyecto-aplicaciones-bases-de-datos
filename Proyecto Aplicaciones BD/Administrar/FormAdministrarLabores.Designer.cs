﻿namespace Proyecto_Aplicaciones_BD.Administrar
{
    partial class FormAdministrarLabores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewLabores = new System.Windows.Forms.DataGridView();
            this.codigoLabor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreLabor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unidadMedida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pagoPorUnidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnBorrarLabor = new System.Windows.Forms.Button();
            this.btnEditarLabor = new System.Windows.Forms.Button();
            this.btnAgregarLabor = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLabores)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewLabores);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(526, 334);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Labores";
            // 
            // dataGridViewLabores
            // 
            this.dataGridViewLabores.AllowUserToAddRows = false;
            this.dataGridViewLabores.AllowUserToDeleteRows = false;
            this.dataGridViewLabores.AllowUserToOrderColumns = true;
            this.dataGridViewLabores.AllowUserToResizeRows = false;
            this.dataGridViewLabores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLabores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codigoLabor,
            this.nombreLabor,
            this.unidadMedida,
            this.pagoPorUnidad});
            this.dataGridViewLabores.Location = new System.Drawing.Point(7, 20);
            this.dataGridViewLabores.Name = "dataGridViewLabores";
            this.dataGridViewLabores.ReadOnly = true;
            this.dataGridViewLabores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewLabores.Size = new System.Drawing.Size(506, 308);
            this.dataGridViewLabores.TabIndex = 0;
            // 
            // codigoLabor
            // 
            this.codigoLabor.DataPropertyName = "codigoLabor";
            this.codigoLabor.HeaderText = "Codigo Labor";
            this.codigoLabor.Name = "codigoLabor";
            this.codigoLabor.ReadOnly = true;
            // 
            // nombreLabor
            // 
            this.nombreLabor.DataPropertyName = "nombreLabor";
            this.nombreLabor.HeaderText = "Nombre de Labor";
            this.nombreLabor.Name = "nombreLabor";
            this.nombreLabor.ReadOnly = true;
            // 
            // unidadMedida
            // 
            this.unidadMedida.DataPropertyName = "unidadMedida";
            this.unidadMedida.HeaderText = "Unidad de Medida";
            this.unidadMedida.Name = "unidadMedida";
            this.unidadMedida.ReadOnly = true;
            // 
            // pagoPorUnidad
            // 
            this.pagoPorUnidad.DataPropertyName = "pagoPorUnidad";
            this.pagoPorUnidad.HeaderText = "Pago x Unidad";
            this.pagoPorUnidad.Name = "pagoPorUnidad";
            this.pagoPorUnidad.ReadOnly = true;
            // 
            // btnBorrarLabor
            // 
            this.btnBorrarLabor.Location = new System.Drawing.Point(552, 90);
            this.btnBorrarLabor.Name = "btnBorrarLabor";
            this.btnBorrarLabor.Size = new System.Drawing.Size(150, 23);
            this.btnBorrarLabor.TabIndex = 7;
            this.btnBorrarLabor.Text = "Borrar Labor";
            this.btnBorrarLabor.UseVisualStyleBackColor = true;
            this.btnBorrarLabor.Click += new System.EventHandler(this.btnBorrarLabor_Click);
            // 
            // btnEditarLabor
            // 
            this.btnEditarLabor.Location = new System.Drawing.Point(552, 61);
            this.btnEditarLabor.Name = "btnEditarLabor";
            this.btnEditarLabor.Size = new System.Drawing.Size(150, 23);
            this.btnEditarLabor.TabIndex = 6;
            this.btnEditarLabor.Text = "Editar Labor";
            this.btnEditarLabor.UseVisualStyleBackColor = true;
            this.btnEditarLabor.Click += new System.EventHandler(this.btnEditarLabor_Click);
            // 
            // btnAgregarLabor
            // 
            this.btnAgregarLabor.Location = new System.Drawing.Point(552, 32);
            this.btnAgregarLabor.Name = "btnAgregarLabor";
            this.btnAgregarLabor.Size = new System.Drawing.Size(150, 23);
            this.btnAgregarLabor.TabIndex = 5;
            this.btnAgregarLabor.Text = "Agregar Labor";
            this.btnAgregarLabor.UseVisualStyleBackColor = true;
            this.btnAgregarLabor.Click += new System.EventHandler(this.btnAgregarLabor_Click);
            // 
            // FormAdministrarLabores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 358);
            this.Controls.Add(this.btnBorrarLabor);
            this.Controls.Add(this.btnEditarLabor);
            this.Controls.Add(this.btnAgregarLabor);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "FormAdministrarLabores";
            this.Text = "Administrar Labores";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLabores)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridViewLabores;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoLabor;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreLabor;
        private System.Windows.Forms.DataGridViewTextBoxColumn unidadMedida;
        private System.Windows.Forms.DataGridViewTextBoxColumn pagoPorUnidad;
        private System.Windows.Forms.Button btnBorrarLabor;
        private System.Windows.Forms.Button btnEditarLabor;
        private System.Windows.Forms.Button btnAgregarLabor;
    }
}