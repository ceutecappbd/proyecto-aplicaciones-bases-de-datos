﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Proyecto_Aplicaciones_BD.Administrar
{
    public partial class FormAdministrarDeducciones : Form
    {

        MySqlConnection Conexion_MySQL;

        public FormAdministrarDeducciones(FormaPrincipal parent, MySqlConnection con)
        {
            InitializeComponent();

            this.MaximumSize = this.MinimumSize = this.Size;
            Conexion_MySQL = con;
            this.MdiParent = parent;

            ActualizarTabla();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            FormAddEditDeduccion form = new FormAddEditDeduccion(Conexion_MySQL);
            form.ShowDialog();
            ActualizarTabla();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dataGridViewDeducciones.SelectedRows.Count > 0)
            {
                FormAddEditDeduccion editDed = new FormAddEditDeduccion(Conexion_MySQL, dataGridViewDeducciones.SelectedRows[0].Cells[0].Value.ToString());
                editDed.ShowDialog();
                ActualizarTabla();
            }
            else
            {
                MessageBox.Show("Debe seleccionar una deduccion");
            }
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (dataGridViewDeducciones.SelectedRows.Count > 0)
            {
                try
                {
                    MySqlCommand query = new MySqlCommand("DELETE FROM deducciones WHERE codDeduccion=@coddeduccion", Conexion_MySQL);
                    query.Parameters.AddWithValue("@coddeduccion", this.dataGridViewDeducciones.SelectedRows[0].Cells[0].Value.ToString());

                    DialogResult resultado = MessageBox.Show("Desea borrar la deduccion?", "Borrar deduccion", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);

                    if (resultado == DialogResult.Yes)
                    {
                        if (query.ExecuteNonQuery() == 1)
                        {
                            MessageBox.Show("Deduccion borrada exitosamente");
                            ActualizarTabla();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ocurrio un error al borrar la deduccion. Mensaje : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                ActualizarTabla();
            }
            else
            {
                MessageBox.Show("Debe seleccionar una deduccion primero");
            }
        }

        private void ActualizarTabla()
        {
            try
            {

                DataTable table = new DataTable();
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter("SELECT * from deducciones", Conexion_MySQL);

                dataAdapter.Fill(table);

                dataGridViewDeducciones.DataSource = table;

            }
            catch (Exception e)
            {
                MessageBox.Show("Error al actualizar tabla de deducciones. Mensaje : " + e.Message, "Error al actualizar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dataGridViewDeducciones_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

        }
    }
}
