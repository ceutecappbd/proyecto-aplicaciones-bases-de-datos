﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Proyecto_Aplicaciones_BD.Administrar
{
    public partial class FormAddEditLabor : Form
    {

        private MySqlConnection Conexion_MySQL;
        private bool editandoLabor = false;
        private String idLabor = "";

        public FormAddEditLabor(MySqlConnection con)
        {
            InitializeComponent();
            this.MaximumSize = this.MinimumSize = this.Size;
            this.Conexion_MySQL = con;
            this.Text = "Agregar Labor";
        }

        public FormAddEditLabor(MySqlConnection con, String laborid)
        {
            InitializeComponent();
            this.MaximumSize = this.MinimumSize = this.Size;
            this.Conexion_MySQL = con;
            IDLaborText.Text = laborid;
            this.Text = "Editar Labor";
            editandoLabor = true;
            idLabor = laborid;
            GetLaborInfo(idLabor);
        }

        private void GetLaborInfo(String id)
        {
            try
            {
                MySqlCommand query = new MySqlCommand("SELECT nombreLabor, unidadMedida, pagoPorUnidad FROM labores WHERE codigoLabor = @codigolabor", Conexion_MySQL);
                query.Parameters.AddWithValue("@codigolabor", id);

                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    NombreLabor.Text = reader["nombreLabor"].ToString();
                    UnidadMedida.Text = reader["unidadMedida"].ToString();
                    PagoUnidad.Text = reader["pagoPorUnidad"].ToString();

                }

                reader.Close();

            }
            catch (Exception)
            {
                MessageBox.Show("Error al obtener informacion para la labor con ID : " + id, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool ValidarCampos()
        {
            if (IDLaborText.Text.Length == 0)
                return false;
            if (NombreLabor.Text.Length == 0)
                return false;
            if (UnidadMedida.Text.Length == 0)
                return false;
            if (PagoUnidad.Text.Length == 0)
                return false;

            double test;
            if (!double.TryParse(PagoUnidad.Text, out test))
                return false;

            return true;

        }

        private bool ActualizarLaborInfo(String codigo, String nombre, String unidad, double pago)
        {
            try
            {
                MySqlCommand query = new MySqlCommand("UPDATE labores SET codigoLabor=@nuevocodigo,nombreLabor=@nombrelabor,unidadMedida=@unidadmedida,pagoPorUnidad=@pagoporunidad WHERE codigoLabor = @codigolabor", Conexion_MySQL);
                query.Parameters.AddWithValue("@codigolabor", idLabor);
                query.Parameters.AddWithValue("@nuevocodigo", codigo);
                query.Parameters.AddWithValue("@nombrelabor", nombre);
                query.Parameters.AddWithValue("@unidadmedida", unidad);
                query.Parameters.AddWithValue("@pagoporunidad", pago);

                if (query.ExecuteNonQuery() == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Excepcion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private bool InsertarNuevaLabor(String codigo, String nombre, String unidad, double pago)
        {
            try
            {
                MySqlCommand query = new MySqlCommand("INSERT INTO labores(codigoLabor, nombreLabor, unidadMedida, pagoPorUnidad) VALUES (@nuevocodigo,@nombrelabor,@unidadmedida,@pagoporunidad)", Conexion_MySQL);
                query.Parameters.AddWithValue("@nuevocodigo", codigo);
                query.Parameters.AddWithValue("@nombrelabor", nombre);
                query.Parameters.AddWithValue("@unidadmedida", unidad);
                query.Parameters.AddWithValue("@pagoporunidad", pago);

                if (query.ExecuteNonQuery() == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Excepcion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if(editandoLabor)
            {
                if(ValidarCampos())
                {
                    if(ActualizarLaborInfo(IDLaborText.Text, NombreLabor.Text, UnidadMedida.Text, double.Parse(PagoUnidad.Text)))
                    {
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Ocurrio un error al actualizar la informacion de la labor", "Error al actualizar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("No pueden quedar campos vacios o el campo de pago por unidad no es un valor numerico");
                }
            }
            else
            {
                if (ValidarCampos())
                {
                    if(InsertarNuevaLabor(IDLaborText.Text, NombreLabor.Text, UnidadMedida.Text, double.Parse(PagoUnidad.Text)))
                    {
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Ocurrio un error al agregar la nueva labor", "Error al agregar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("No pueden quedar campos vacios o el campo de pago por unidad no es un valor numerico");
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
