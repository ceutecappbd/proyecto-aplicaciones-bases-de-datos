﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Proyecto_Aplicaciones_BD.Administrar
{
    public partial class FormAddEditEmpleado : Form
    {

        private MySqlConnection Conexion_MySQL;
        private bool editandoEmpleado = false;
        private int idEmpleado = -1;

        public FormAddEditEmpleado(MySqlConnection con)
        {
            InitializeComponent();
            this.MaximumSize = this.MinimumSize = this.Size;
            this.Conexion_MySQL = con;
            this.Text = "Agregar Empleado";
        }

        public FormAddEditEmpleado(MySqlConnection con, int ID)
        {
            InitializeComponent();
            this.MaximumSize = this.MinimumSize = this.Size;
            this.Conexion_MySQL = con;
            IDTextbox.Text = ID.ToString();
            this.Text = "Editar Empleado";
            editandoEmpleado = true;
            idEmpleado = ID;
            GetEmpleadoInfo(idEmpleado);
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            if(editandoEmpleado)
            {
                if(ValidarCampos())
                {
                    if(ActualizarEmpleadoInfo(NameTextBox.Text, LastNameTextbox.Text, NacimientoPicker.Value.Date.ToString("yyyy-MM-dd"), IdentidadTextBox.Text, TelefonoTextBox.Text, CuentaTextbox.Text))
                    {
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Ocurrio un error al actualizar la informacion del empleado", "Error al actualizar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("No pueden quedar campos vacios a excepcion de la cuenta bancaria");
                }
            }
            else
            {
                if(ValidarCampos())
                {
                    if(InsertarNuevoEmpleado(NameTextBox.Text, LastNameTextbox.Text, NacimientoPicker.Value.Date.ToString("yyyy-MM-dd"), IdentidadTextBox.Text, TelefonoTextBox.Text, CuentaTextbox.Text))
                    {
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Ocurrio un error al ingresar la informacion del empleado", "Error al actualizar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("No pueden quedar campos vacios a excepcion de la cuenta bancaria");
                }
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GetEmpleadoInfo(int idempleado)
        {
            try
            {
                MySqlCommand query = new MySqlCommand("SELECT nombreEmpleado, apellidoEmpleado, fechaNacimiento, numeroIdentidad, numeroTelefonico, cuentaBancaria FROM empleados WHERE idEmpleado = @idempleado", Conexion_MySQL);
                query.Parameters.AddWithValue("@idempleado", idempleado);

                MySqlDataReader reader = query.ExecuteReader();

                if(reader.Read())
                {
                    NameTextBox.Text = reader["nombreEmpleado"].ToString();
                    LastNameTextbox.Text = reader["apellidoEmpleado"].ToString();

                    //Convertir el string de fecha de nacimiento a algo que el datepicker entienda
                    String fecha = reader["fechaNacimiento"].ToString();
                    NacimientoPicker.Value = DateTime.Parse(fecha);
                    Console.WriteLine(fecha);
                    IdentidadTextBox.Text = reader["numeroIdentidad"].ToString();
                    TelefonoTextBox.Text = reader["numeroTelefonico"].ToString();
                    CuentaTextbox.Text = reader["cuentaBancaria"].ToString();
                }


                reader.Close();

            }
            catch(Exception)
            {
                MessageBox.Show("Error al obtener informacion para el empleado con ID : " + idempleado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool ActualizarEmpleadoInfo(String nombres, String apellidos, String fechaNacimiento, String numeroIdentidad, String numeroTelefonico, String cuentaBancaria)
        {

            try
            {
                MySqlCommand query = new MySqlCommand("UPDATE empleados SET nombreEmpleado=@nombreempleado,apellidoEmpleado=@apellidoempleado,fechaNacimiento=@fechanacimiento,numeroIdentidad=@numeroidentidad,numeroTelefonico=@numerotelefonico,cuentaBancaria=@cuentabancaria WHERE idEmpleado = @idempleado", Conexion_MySQL);
                query.Parameters.AddWithValue("@nombreempleado", nombres);
                query.Parameters.AddWithValue("@apellidoempleado", apellidos);
                query.Parameters.AddWithValue("@fechanacimiento", fechaNacimiento);
                query.Parameters.AddWithValue("@numeroidentidad", numeroIdentidad);
                query.Parameters.AddWithValue("@numerotelefonico", numeroTelefonico);
                query.Parameters.AddWithValue("@cuentabancaria", cuentaBancaria);
                query.Parameters.AddWithValue("@idempleado", idEmpleado);

                if(query.ExecuteNonQuery()==1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Excepcion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        private bool InsertarNuevoEmpleado(String nombres, String apellidos, String fechaNacimiento, String numeroIdentidad, String numeroTelefonico, String cuentaBancaria)
        {

            try
            {
                MySqlCommand query = new MySqlCommand("INSERT into empleados(nombreEmpleado, apellidoEmpleado, fechaNacimiento, numeroIdentidad, numeroTelefonico, cuentaBancaria) VALUES(@nombreempleado,@apellidoempleado,@fechanacimiento,@numeroidentidad,@numerotelefonico,@cuentabancaria)", Conexion_MySQL);
                query.Parameters.AddWithValue("@nombreempleado", nombres);
                query.Parameters.AddWithValue("@apellidoempleado", apellidos);
                query.Parameters.AddWithValue("@fechanacimiento", fechaNacimiento);
                query.Parameters.AddWithValue("@numeroidentidad", numeroIdentidad);
                query.Parameters.AddWithValue("@numerotelefonico", numeroTelefonico);
                query.Parameters.AddWithValue("@cuentabancaria", cuentaBancaria);
                query.Parameters.AddWithValue("@idempleado", idEmpleado);

                if(query.ExecuteNonQuery()==1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Excepcion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        private bool ValidarCampos()
        {
            if (NameTextBox.Text.Length == 0)
                return false;
            if (LastNameTextbox.Text.Length == 0)
                return false;
            if (IdentidadTextBox.Text.Length == 0)
                return false;
            if (TelefonoTextBox.Text.Length == 0)
                return false;

            return true;
        }
    }
}
