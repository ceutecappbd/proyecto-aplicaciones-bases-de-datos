﻿namespace Proyecto_Aplicaciones_BD.Administrar
{
    partial class AdministrarCalendarioActividades
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.calendario = new System.Windows.Forms.MonthCalendar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.weekNumberTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateEmpiezaSemana = new System.Windows.Forms.DateTimePicker();
            this.dateTerminaSemana = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewInfo = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.labelTarea = new System.Windows.Forms.ToolStripStatusLabel();
            this.progresoTarea = new System.Windows.Forms.ToolStripProgressBar();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.nombreEmpleado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idEmpleado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idCalendario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idCalendarioDetail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tareaDomingo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tareaLunes = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tareaMartes = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tareaMiercoles = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tareaJueves = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tareaViernes = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tareaSabado = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.lockRow = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInfo)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // calendario
            // 
            this.calendario.FirstDayOfWeek = System.Windows.Forms.Day.Sunday;
            this.calendario.Location = new System.Drawing.Point(12, 25);
            this.calendario.Name = "calendario";
            this.calendario.ShowWeekNumbers = true;
            this.calendario.TabIndex = 0;
            this.calendario.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.calendario_DateSelected);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.calendario);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(274, 206);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seleccion un dia de la semana que quiere programar";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Numero de la Semana :";
            // 
            // weekNumberTextbox
            // 
            this.weekNumberTextbox.Enabled = false;
            this.weekNumberTextbox.Location = new System.Drawing.Point(142, 27);
            this.weekNumberTextbox.Name = "weekNumberTextbox";
            this.weekNumberTextbox.ReadOnly = true;
            this.weekNumberTextbox.Size = new System.Drawing.Size(20, 20);
            this.weekNumberTextbox.TabIndex = 3;
            this.weekNumberTextbox.Text = "00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Semana Empieza :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Semana Termina :";
            // 
            // dateEmpiezaSemana
            // 
            this.dateEmpiezaSemana.Enabled = false;
            this.dateEmpiezaSemana.Location = new System.Drawing.Point(142, 53);
            this.dateEmpiezaSemana.Name = "dateEmpiezaSemana";
            this.dateEmpiezaSemana.Size = new System.Drawing.Size(200, 20);
            this.dateEmpiezaSemana.TabIndex = 6;
            // 
            // dateTerminaSemana
            // 
            this.dateTerminaSemana.Enabled = false;
            this.dateTerminaSemana.Location = new System.Drawing.Point(142, 84);
            this.dateTerminaSemana.Name = "dateTerminaSemana";
            this.dateTerminaSemana.Size = new System.Drawing.Size(200, 20);
            this.dateTerminaSemana.TabIndex = 7;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.dateTerminaSemana);
            this.groupBox2.Controls.Add(this.weekNumberTextbox);
            this.groupBox2.Controls.Add(this.dateEmpiezaSemana);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(311, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(598, 206);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Informacion de la Semana";
            // 
            // dataGridViewInfo
            // 
            this.dataGridViewInfo.AllowUserToAddRows = false;
            this.dataGridViewInfo.AllowUserToDeleteRows = false;
            this.dataGridViewInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nombreEmpleado,
            this.idEmpleado,
            this.idCalendario,
            this.idCalendarioDetail,
            this.tareaDomingo,
            this.tareaLunes,
            this.tareaMartes,
            this.tareaMiercoles,
            this.tareaJueves,
            this.tareaViernes,
            this.tareaSabado,
            this.lockRow});
            this.dataGridViewInfo.Enabled = false;
            this.dataGridViewInfo.Location = new System.Drawing.Point(6, 19);
            this.dataGridViewInfo.MultiSelect = false;
            this.dataGridViewInfo.Name = "dataGridViewInfo";
            this.dataGridViewInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewInfo.Size = new System.Drawing.Size(885, 338);
            this.dataGridViewInfo.TabIndex = 9;
            this.dataGridViewInfo.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewInfo_CellValueChanged);
            this.dataGridViewInfo.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridViewInfo_RowsAdded);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridViewInfo);
            this.groupBox3.Location = new System.Drawing.Point(12, 224);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(897, 363);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Empleados y Tareas a Programar";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.labelTarea,
            this.progresoTarea});
            this.statusStrip1.Location = new System.Drawing.Point(0, 620);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(921, 22);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(45, 17);
            this.toolStripStatusLabel1.Text = "Tarea : ";
            // 
            // labelTarea
            // 
            this.labelTarea.Name = "labelTarea";
            this.labelTarea.Size = new System.Drawing.Size(53, 17);
            this.labelTarea.Text = "Ninguna";
            // 
            // progresoTarea
            // 
            this.progresoTarea.Name = "progresoTarea";
            this.progresoTarea.Size = new System.Drawing.Size(100, 16);
            this.progresoTarea.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // btnCancelar
            // 
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Location = new System.Drawing.Point(814, 594);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 12;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(689, 594);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(111, 23);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "Guardar Calendario";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // nombreEmpleado
            // 
            this.nombreEmpleado.DataPropertyName = "nombreCompleto";
            this.nombreEmpleado.HeaderText = "Nombre Empleado";
            this.nombreEmpleado.Name = "nombreEmpleado";
            // 
            // idEmpleado
            // 
            this.idEmpleado.DataPropertyName = "idEmpleado";
            this.idEmpleado.HeaderText = "idEmpleado";
            this.idEmpleado.Name = "idEmpleado";
            this.idEmpleado.Visible = false;
            // 
            // idCalendario
            // 
            this.idCalendario.DataPropertyName = "idCalendario";
            this.idCalendario.HeaderText = "idCalendario";
            this.idCalendario.Name = "idCalendario";
            this.idCalendario.Visible = false;
            // 
            // idCalendarioDetail
            // 
            this.idCalendarioDetail.DataPropertyName = "idCalendarioDetail";
            this.idCalendarioDetail.HeaderText = "idCalendarioDetail";
            this.idCalendarioDetail.Name = "idCalendarioDetail";
            this.idCalendarioDetail.Visible = false;
            // 
            // tareaDomingo
            // 
            this.tareaDomingo.DataPropertyName = "codigoLaborDomingo";
            this.tareaDomingo.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.tareaDomingo.HeaderText = "Tarea Domingo";
            this.tareaDomingo.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.tareaDomingo.MaxDropDownItems = 100;
            this.tareaDomingo.Name = "tareaDomingo";
            // 
            // tareaLunes
            // 
            this.tareaLunes.DataPropertyName = "codigoLaborLunes";
            this.tareaLunes.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.tareaLunes.HeaderText = "Tarea Lunes";
            this.tareaLunes.Name = "tareaLunes";
            // 
            // tareaMartes
            // 
            this.tareaMartes.DataPropertyName = "codigoLaborMartes";
            this.tareaMartes.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.tareaMartes.HeaderText = "Tarea Martes";
            this.tareaMartes.Name = "tareaMartes";
            // 
            // tareaMiercoles
            // 
            this.tareaMiercoles.DataPropertyName = "codigoLaborMiercoles";
            this.tareaMiercoles.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.tareaMiercoles.HeaderText = "Tarea Miercoles";
            this.tareaMiercoles.Name = "tareaMiercoles";
            // 
            // tareaJueves
            // 
            this.tareaJueves.DataPropertyName = "codigoLaborJueves";
            this.tareaJueves.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.tareaJueves.HeaderText = "Tarea Jueves";
            this.tareaJueves.Name = "tareaJueves";
            // 
            // tareaViernes
            // 
            this.tareaViernes.DataPropertyName = "codigoLaborViernes";
            this.tareaViernes.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.tareaViernes.HeaderText = "Tarea Viernes";
            this.tareaViernes.Name = "tareaViernes";
            // 
            // tareaSabado
            // 
            this.tareaSabado.DataPropertyName = "codigoLaborSabado";
            this.tareaSabado.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.tareaSabado.HeaderText = "Tarea Sabado";
            this.tareaSabado.Name = "tareaSabado";
            // 
            // lockRow
            // 
            this.lockRow.DataPropertyName = "lock";
            this.lockRow.FalseValue = "0";
            this.lockRow.HeaderText = "Lock";
            this.lockRow.Name = "lockRow";
            this.lockRow.ReadOnly = true;
            this.lockRow.TrueValue = "1";
            // 
            // AdministrarCalendarioActividades
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(921, 642);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "AdministrarCalendarioActividades";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Administrar Calendario de Actividades";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInfo)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar calendario;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox weekNumberTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateEmpiezaSemana;
        private System.Windows.Forms.DateTimePicker dateTerminaSemana;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridViewInfo;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar progresoTarea;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.ToolStripStatusLabel labelTarea;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreEmpleado;
        private System.Windows.Forms.DataGridViewTextBoxColumn idEmpleado;
        private System.Windows.Forms.DataGridViewTextBoxColumn idCalendario;
        private System.Windows.Forms.DataGridViewTextBoxColumn idCalendarioDetail;
        private System.Windows.Forms.DataGridViewComboBoxColumn tareaDomingo;
        private System.Windows.Forms.DataGridViewComboBoxColumn tareaLunes;
        private System.Windows.Forms.DataGridViewComboBoxColumn tareaMartes;
        private System.Windows.Forms.DataGridViewComboBoxColumn tareaMiercoles;
        private System.Windows.Forms.DataGridViewComboBoxColumn tareaJueves;
        private System.Windows.Forms.DataGridViewComboBoxColumn tareaViernes;
        private System.Windows.Forms.DataGridViewComboBoxColumn tareaSabado;
        private System.Windows.Forms.DataGridViewCheckBoxColumn lockRow;

    }
}