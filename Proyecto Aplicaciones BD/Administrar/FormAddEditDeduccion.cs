﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Proyecto_Aplicaciones_BD.Administrar
{
    public partial class FormAddEditDeduccion : Form
    {

        private MySqlConnection Conexion_MySQL;
        private bool editandoDeduccion = false;
        private string codDeduccion = "";
        public FormAddEditDeduccion(MySqlConnection con)
        {
            InitializeComponent();
            this.MaximumSize = this.MinimumSize = this.Size;
            this.Conexion_MySQL = con;
            this.Text = "Agregar Deduccion";
            tipoDeduccion.SelectedIndex = 0;
        }

        public FormAddEditDeduccion(MySqlConnection con, string codigo)
        {
            InitializeComponent();
            this.MaximumSize = this.MinimumSize = this.Size;
            this.Conexion_MySQL = con;
            this.Text = "Editando Deduccion";
            this.editandoDeduccion = true;
            this.codDeduccion = codigo;
            tipoDeduccion.SelectedIndex = 0;
            GetDeduccionInfo(codigo);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if(editandoDeduccion)
            {
                if(ValidarCampos())
                {

                   if(ActualizarDeduccion(codigoTextbox.Text, tipoDeduccion.SelectedIndex, double.Parse(valorTextbox.Text)))
                   {
                       this.Close();
                   }
                   else
                   {
                       MessageBox.Show("Ocurrio un error al actualizar la informacion de la deduccion", "Error al actualizar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                   }

                }
                else
                {
                    MessageBox.Show("No pueden quedar campos vacios.");
                }
            }
            else
            {
                if(ValidarCampos())
                {
                     if(InsertarNuevaDeduccion(codigoTextbox.Text, tipoDeduccion.SelectedIndex, double.Parse(valorTextbox.Text)))
                    {
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Ocurrio un error al ingresar la informacion de la deduccion", "Error al actualizar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("No pueden quedar campos vacios.");
                }
            }
        }

        private void GetDeduccionInfo(string codigo)
        {
            try
            {
                MySqlCommand query = new MySqlCommand("SELECT codDeduccion,tipoDeduccion,valorDeduccion FROM deducciones WHERE codDeduccion = @codDeduccion", Conexion_MySQL);
                query.Parameters.AddWithValue("@codDeduccion", codigo);

                MySqlDataReader reader = query.ExecuteReader();

                if (reader.Read())
                {
                    codigoTextbox.Text = reader["codDeduccion"].ToString();
                    tipoDeduccion.SelectedIndex = int.Parse(reader["tipoDeduccion"].ToString());
                    valorTextbox.Text = reader["valorDeduccion"].ToString();
                    codigoTextbox.Enabled = false;
                }

                reader.Close();

            }
            catch (Exception e)
            {
                MessageBox.Show("Error al obtener informacion para la deduccion con Codigo : " + codigo, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(e.Message);
            }
        }

        private bool ActualizarDeduccion(String codigo, int tipo, double valor)
        {

            try
            {
                MySqlCommand query = new MySqlCommand("UPDATE deducciones SET tipoDeduccion=@tipoDeduccion, valorDeduccion=@valorDeduccion WHERE codDeduccion=@codDeduccion", Conexion_MySQL);
                query.Parameters.AddWithValue("@codDeduccion", codigo);
                query.Parameters.AddWithValue("@tipoDeduccion", tipo);
                query.Parameters.AddWithValue("@valorDeduccion", valor);

                if (query.ExecuteNonQuery() == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Excepcion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        private bool InsertarNuevaDeduccion(String codigo, int tipo, double valor)
        {

            try
            {
                MySqlCommand query = new MySqlCommand("INSERT into deducciones(codDeduccion, tipoDeduccion, valorDeduccion) VALUES(@codDeduccion,@tipoDeduccion,@valorDeduccion)", Conexion_MySQL);
                query.Parameters.AddWithValue("@codDeduccion", codigo);
                query.Parameters.AddWithValue("@tipoDeduccion", tipo);
                query.Parameters.AddWithValue("@valorDeduccion", valor);

                if (query.ExecuteNonQuery() == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Excepcion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        private bool ValidarCampos()
        {
            if (codigoTextbox.Text.Length == 0)
                return false;
            if (valorTextbox.Text.Length == 0)
                return false;
            if (tipoDeduccion.SelectedIndex == -1)
                return false;

            return true;
        }

        private void tipoDeduccion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(tipoDeduccion.SelectedIndex == 0)
            {
                valorTextbox.MaxLength = 20;
            }
            else if(tipoDeduccion.SelectedIndex == 1)
            {
                valorTextbox.MaxLength = 3;
            }
        }
    }
}
