﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Proyecto_Aplicaciones_BD.Administrar
{
    public partial class AdministrarCalendarioActividades : Form
    {

        MySqlConnection Conexion_MySQL;
        DataTable laboresDT;
        bool validated = true;

        public AdministrarCalendarioActividades(FormaPrincipal parent, MySqlConnection con)
        {
            InitializeComponent();
            this.Conexion_MySQL = con;
            ObtenerListaDeLabores();
            this.MdiParent = parent;
            this.MaximumSize = this.MinimumSize = this.Size;

            UpdateCalendarSelection();
        }

        private void calendario_DateSelected(object sender, DateRangeEventArgs e)
        {
            UpdateCalendarSelection();
        }

        private void UpdateCalendarSelection()
        {

            this.progresoTarea.MarqueeAnimationSpeed = 30;
            this.labelTarea.Text = "Actualizando informacion de semana...";

            CultureInfo culture = CultureInfo.CurrentCulture;
            int numeroDeSemana = culture.Calendar.GetWeekOfYear(calendario.SelectionStart, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
            weekNumberTextbox.Text = numeroDeSemana.ToString();
            DateTime semanaEmpieza = FirstDateOfWeek(calendario.SelectionStart.Year, numeroDeSemana, culture);
            DateTime semanaTermina = semanaEmpieza.AddDays(6);
            calendario.SelectionRange.Start = semanaEmpieza;
            calendario.SelectionRange.End = semanaTermina;
            dateEmpiezaSemana.Value = semanaEmpieza;
            dateTerminaSemana.Value = semanaTermina;

            ObtenerInformacionDeSemana();

            this.labelTarea.Text = "Ninguna";
            this.progresoTarea.MarqueeAnimationSpeed = 0;
        }

        private void ObtenerInformacionDeSemana()
        {
            try
            {
                DataTable dt = new DataTable();

                MySqlCommand comando = new MySqlCommand("SELECT calendario.idCalendario, calendario.lock, calendariodetail.idCalendarioDetail, calendariodetail.idEmpleado, CONCAT(empleados.nombreEmpleado,' ',empleados.apellidoEmpleado) AS nombreCompleto, calendariodetail.codigoLaborLunes, calendariodetail.codigoLaborMartes, calendariodetail.codigoLaborMiercoles, calendariodetail.codigoLaborJueves, calendariodetail.codigoLaborViernes, calendariodetail.codigoLaborSabado, calendariodetail.codigoLaborDomingo FROM calendario INNER JOIN calendariodetail ON calendario.idCalendario=calendariodetail.idCalendario INNER JOIN empleados ON calendariodetail.idEmpleado=empleados.idEmpleado WHERE calendario.trabajoDesde = @trabajoDesde AND calendario.trabajoHasta = @trabajoHasta;", Conexion_MySQL);
                comando.Parameters.AddWithValue("@trabajoDesde", this.dateEmpiezaSemana.Value.Date.ToString("yyyy-MM-dd"));
                comando.Parameters.AddWithValue("@trabajoHasta", this.dateTerminaSemana.Value.Date.ToString("yyyy-MM-dd"));

                MySqlDataAdapter adapter = new MySqlDataAdapter(comando);

                adapter.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    dataGridViewInfo.DataSource = dt;
                    dataGridViewInfo.Enabled = true;
                }
                else
                {
                        this.labelTarea.Text = "Creando calendario...";

                        MySqlCommand comandoCrear = new MySqlCommand("INSERT INTO calendario(trabajoDesde,trabajoHasta) VALUES(@desde,@hasta)", Conexion_MySQL);
                        comandoCrear.Parameters.AddWithValue("@desde", this.dateEmpiezaSemana.Value.Date.ToString("yyyy-MM-dd"));
                        comandoCrear.Parameters.AddWithValue("@hasta", this.dateTerminaSemana.Value.Date.ToString("yyyy-MM-dd"));

                        if(comandoCrear.ExecuteNonQuery()>0)
                        {
                            long calendarioID = comandoCrear.LastInsertedId;

                            this.labelTarea.Text = "Detallando calendario para empleados...";
                            MySqlCommand crearScheduleProcedure = new MySqlCommand("crear_calendario_semanal", Conexion_MySQL);
                            crearScheduleProcedure.CommandType = CommandType.StoredProcedure;
                            crearScheduleProcedure.Parameters.AddWithValue("@idCalendario", calendarioID);

                            if(crearScheduleProcedure.ExecuteNonQuery()>0)
                            {
                                this.labelTarea.Text = "Calendario Creado";
                                adapter = new MySqlDataAdapter(comando);
                                adapter.Fill(dt);
                                dataGridViewInfo.DataSource = dt;
                                dataGridViewInfo.Enabled = true;
                            }
                        }
                }
            }
            catch(Exception e)
            {
                MessageBox.Show("Error al actualizar calendario de tareas. Mensaje : " + e.ToString(), "Error al actualizar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ObtenerListaDeLabores()
        {
            try
            {
                laboresDT = new DataTable();
                MySqlDataAdapter adapter = new MySqlDataAdapter("SELECT codigoLabor, nombreLabor FROM labores", Conexion_MySQL);
                adapter.Fill(laboresDT);   
            }
            catch(Exception e)
            {
                MessageBox.Show("Error al actualizar lista de labores. Mensaje : " + e.ToString(), "Error al actualizar", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        public static DateTime FirstDateOfWeek(int year, int weekOfYear, System.Globalization.CultureInfo ci)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = (int)ci.DateTimeFormat.FirstDayOfWeek - (int)jan1.DayOfWeek;
            DateTime firstWeekDay = jan1.AddDays(daysOffset);
            int firstWeek = ci.Calendar.GetWeekOfYear(jan1, ci.DateTimeFormat.CalendarWeekRule, ci.DateTimeFormat.FirstDayOfWeek);
            if (firstWeek <= 1 || firstWeek > 50)
            {
                weekOfYear -= 1;
            }
            return firstWeekDay.AddDays(weekOfYear * 7);
        }


        private void dataGridViewInfo_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            foreach (DataGridViewCell cell in dataGridViewInfo.Rows[e.RowIndex].Cells)
            {
                if(cell is DataGridViewComboBoxCell)
                { 
                    ((DataGridViewComboBoxCell)cell).DataSource = laboresDT;
                    ((DataGridViewComboBoxCell)cell).DisplayMember = "nombreLabor";
                    ((DataGridViewComboBoxCell)cell).ValueMember = "codigoLabor";
                }
            }

        }

        private void dataGridViewInfo_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewInfo.Rows.Count > 0)
            {
                foreach (DataGridViewCell cell in dataGridViewInfo.Rows[e.RowIndex].Cells)
                {
                    if (cell is DataGridViewComboBoxCell)
                    {
                        foreach (DataGridViewCell cell2 in dataGridViewInfo.Rows[e.RowIndex].Cells)
                        {
                            if (cell2 is DataGridViewComboBoxCell && cell2 != cell)
                            {
                                if (((DataGridViewComboBoxCell)cell).Value == ((DataGridViewComboBoxCell)cell2).Value)
                                {
                                    validated = false;
                                    Console.WriteLine("False." + ((DataGridViewComboBoxCell)cell).Value + "==" + ((DataGridViewComboBoxCell)cell2).Value);
                                    MessageBox.Show("No puede repetir labores dentro de la semana!!! Corrijalo para poder guardar.");
                                    break;
                                }
                                else
                                {
                                    Console.WriteLine("True");
                                    validated = true;
                                }
                            }
                        }
                        break;
                    }

                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if(dataGridViewInfo.Rows.Count > 0 && validated)
            {
                labelTarea.Text = "Guardando calendario de actividades...";
                progresoTarea.MarqueeAnimationSpeed = 30;
                int conteoRegistrosGuardados = 0;
                foreach(DataGridViewRow row in dataGridViewInfo.Rows)
                {
                    try
                    {
                        Console.WriteLine("Guardando registro con idCalendarioDetail : " + row.Cells["idCalendarioDetail"].Value.ToString() + " con idEmpleado : " + row.Cells["idEmpleado"].Value.ToString());
                        MySqlCommand comandoguardarregistro = new MySqlCommand("UPDATE calendariodetail SET codigoLaborLunes=@lunes,codigoLaborMartes=@martes,codigoLaborMiercoles=@miercoles,codigoLaborJueves=@jueves,codigoLaborViernes=@viernes,codigoLaborSabado=@sabado,codigoLaborDomingo=@domingo WHERE idCalendarioDetail=@idDetail AND idEmpleado=@idEmpleado", Conexion_MySQL);
                        comandoguardarregistro.Parameters.AddWithValue("@lunes", SanitizeString(row.Cells["tareaLunes"].Value.ToString()));
                        comandoguardarregistro.Parameters.AddWithValue("@martes", SanitizeString(row.Cells["tareaMartes"].Value.ToString()));
                        comandoguardarregistro.Parameters.AddWithValue("@miercoles", SanitizeString(row.Cells["tareaMiercoles"].Value.ToString()));
                        comandoguardarregistro.Parameters.AddWithValue("@jueves", SanitizeString(row.Cells["tareaJueves"].Value.ToString()));
                        comandoguardarregistro.Parameters.AddWithValue("@viernes", SanitizeString(row.Cells["tareaViernes"].Value.ToString()));
                        comandoguardarregistro.Parameters.AddWithValue("@sabado", SanitizeString(row.Cells["tareaSabado"].Value.ToString()));
                        comandoguardarregistro.Parameters.AddWithValue("@domingo", SanitizeString(row.Cells["tareaDomingo"].Value.ToString()));
                        comandoguardarregistro.Parameters.AddWithValue("@idDetail", row.Cells["idCalendarioDetail"].Value.ToString());
                        comandoguardarregistro.Parameters.AddWithValue("@idEmpleado", row.Cells["idEmpleado"].Value.ToString());
                    
                        if(comandoguardarregistro.ExecuteNonQuery()>0)
                        {
                            conteoRegistrosGuardados += 1;
                        }
                        
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine("Error al guardar calendario detail. Error : " + ex.ToString());
                    }
                }

                if (conteoRegistrosGuardados == dataGridViewInfo.Rows.Count)
                {
                    labelTarea.Text = "Calendario guardado exitosamente";
                    progresoTarea.MarqueeAnimationSpeed = 0;
                }
                else
                {
                    labelTarea.Text = "Calendario guardado con unos errores. Por favor chequear el registro del sistema.";
                    progresoTarea.MarqueeAnimationSpeed = 0;
                }

            }
        }

        private object SanitizeString(String str)
        {
            if(str == "" || str.Length == 0)
            {
                Console.WriteLine("Empty String");
                return DBNull.Value;
            }
            else
            {
                return str;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
