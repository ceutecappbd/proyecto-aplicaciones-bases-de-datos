﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

//Para usar el MySQL necesitan incluir las librerias o namespaces
using MySql.Data.MySqlClient;

//Para usar clases dentro de los 'folders' (Administrar, Planillas, Entidades BD, Reportes) hacer lo siguiente
using Proyecto_Aplicaciones_BD.Entidades_BD;
using Proyecto_Aplicaciones_BD.Administrar;
using Proyecto_Aplicaciones_BD.Planillas;
using Proyecto_Aplicaciones_BD.Reportes;


namespace Proyecto_Aplicaciones_BD
{
    public partial class FormaPrincipal : Form
    {

        //Declaran una variable de tipo MySQLClient para empezar a hacer conexiones.
        private MySqlConnection Conexion_MySQL;
        private Admin administrador;
        private LoginForm login;

        #region Getters y Setters
        public MySqlConnection Conexion
        {
            get
            {
                return this.Conexion_MySQL;
            }
        }

        public Admin Administrador
        {
            get
            {
                return administrador;
            }
            set
            {
                administrador = value;
            }
        }

        #endregion

        public FormaPrincipal()
        {
            InitializeComponent();

            //Si no logramos iniciar una conexion a la base de datos que se cierre la app.
            if (!ConectarMySQL())
                Environment.Exit(-1);

            ShowLoginForm();
            
        }

        public void LoginSuccesful()
        {
            //Codigo utilizado para cerrar la ventana abierta de login, habilitar el menu y colocar el nombre
            //del usuario conectado en el status strip de abajo.
            this.mysqlstatusstrip.Text = "Usuario conectado : " + Administrador.Nombre + " " + Administrador.Apellido;
            this.menuStrip.Enabled = true;
            login.Close();
            login.Dispose();
            login = null;
        }

        private void ShowLoginForm()
        {
            //Limpiamos el usuario conectado y bloqueamos la ventana para que no pueda ser utilizada.
            //Posteriormente mostramos una nueva ventana de login.
            this.menuStrip.Enabled = false;
            this.mysqlstatusstrip.Text = "Usuario conectado : Ninguno";
            this.Administrador = null;

            //No crear mas ventanas de login si ya tenemos una
            if(login==null)
                login = new LoginForm(this, Conexion);

            login.StartPosition = FormStartPosition.CenterScreen;
            login.Show();
        }

        //Codigo utilizado para conectarse a la base de datos. Todo codigo que intente hacer una conexion
        //o query a la base de datos no debe pero se recomienda altamente que este entre un try/catch
        //para evitar que se cuelgue la aplicacion.
        private bool ConectarMySQL()
        {
            
            //String utilizado para conectarse a la base de datos.
            Conexion_MySQL = new MySqlConnection("Server=127.0.0.1;Database=proyecto;Uid=root;Password=12345678");

            try
            {
                Conexion_MySQL.Open();
                return true;
            }
            catch(MySqlException e)
            {
                MessageBox.Show("No se pudo conectar con la base de datos.", "Error en la conexion con la base de datos.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show(e.Message, "Excepcion MySQL", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FormaPrincipal_Load(object sender, EventArgs e)
        {
            
        }

        private void cerrarSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowLoginForm();
        }

        private void gestionarEmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAdministrarEmpleados form = new FormAdministrarEmpleados(this, Conexion);
            form.Show();
        }

        private void gestionarLaboresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAdministrarLabores form = new FormAdministrarLabores(this, Conexion);
            form.Show();
        }

        private void gestionarDeduccionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAdministrarDeducciones form = new FormAdministrarDeducciones(this, Conexion);
            form.Show();
        }

        private void gestionarCalendarioDeLaboresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdministrarCalendarioActividades form = new AdministrarCalendarioActividades(this, Conexion);
            form.Show();
        }

        private void reportePlanillaSemanalToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void reporteLaboresPorSemanaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void generarPlanillaDePagosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GenerarPlanilla form = new GenerarPlanilla(this, Conexion);
            form.Show();
        }
    }
}
