﻿namespace Proyecto_Aplicaciones_BD.Planillas
{
    partial class GenerarPlanilla
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxSemanaTrabajo = new System.Windows.Forms.ComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.labelTarea = new System.Windows.Forms.ToolStripStatusLabel();
            this.progreso = new System.Windows.Forms.ToolStripProgressBar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewPlanilla = new System.Windows.Forms.DataGridView();
            this.btnObtenerDatosPlanilla = new System.Windows.Forms.Button();
            this.idPlanilla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idCalendario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subtotalPlanilla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalDeducciones = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalPlanilla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idPlanillaDetail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idCalendarioDetail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreCompleto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laborLunes = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cantLunes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laborMartes = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cantMartes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laborMiercoles = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cantMiercoles = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laborJueves = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cantJueves = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laborViernes = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cantViernes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laborSabado = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cantSabado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laborDomingo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cantDomingo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subtotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deducciones = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlanilla)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnObtenerDatosPlanilla);
            this.groupBox1.Controls.Add(this.comboBoxSemanaTrabajo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(890, 71);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Escoger planilla a generar";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(173, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Semana de Trabajo : ";
            // 
            // comboBoxSemanaTrabajo
            // 
            this.comboBoxSemanaTrabajo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSemanaTrabajo.FormattingEnabled = true;
            this.comboBoxSemanaTrabajo.Location = new System.Drawing.Point(288, 30);
            this.comboBoxSemanaTrabajo.Name = "comboBoxSemanaTrabajo";
            this.comboBoxSemanaTrabajo.Size = new System.Drawing.Size(346, 21);
            this.comboBoxSemanaTrabajo.TabIndex = 1;
            this.comboBoxSemanaTrabajo.SelectedIndexChanged += new System.EventHandler(this.comboBoxSemanaTrabajo_SelectedIndexChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.labelTarea,
            this.progreso});
            this.statusStrip1.Location = new System.Drawing.Point(0, 454);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(915, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(45, 17);
            this.toolStripStatusLabel1.Text = "Tarea : ";
            // 
            // labelTarea
            // 
            this.labelTarea.Name = "labelTarea";
            this.labelTarea.Size = new System.Drawing.Size(53, 17);
            this.labelTarea.Text = "Ninguna";
            // 
            // progreso
            // 
            this.progreso.Name = "progreso";
            this.progreso.Size = new System.Drawing.Size(100, 16);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridViewPlanilla);
            this.groupBox2.Location = new System.Drawing.Point(13, 90);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(890, 324);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Informacion de Planilla";
            // 
            // dataGridViewPlanilla
            // 
            this.dataGridViewPlanilla.AllowUserToAddRows = false;
            this.dataGridViewPlanilla.AllowUserToDeleteRows = false;
            this.dataGridViewPlanilla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPlanilla.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idPlanilla,
            this.idCalendario,
            this.subtotalPlanilla,
            this.totalDeducciones,
            this.totalPlanilla,
            this.idPlanillaDetail,
            this.idCalendarioDetail,
            this.nombreCompleto,
            this.laborLunes,
            this.cantLunes,
            this.laborMartes,
            this.cantMartes,
            this.laborMiercoles,
            this.cantMiercoles,
            this.laborJueves,
            this.cantJueves,
            this.laborViernes,
            this.cantViernes,
            this.laborSabado,
            this.cantSabado,
            this.laborDomingo,
            this.cantDomingo,
            this.subtotal,
            this.deducciones,
            this.total});
            this.dataGridViewPlanilla.Location = new System.Drawing.Point(6, 19);
            this.dataGridViewPlanilla.Name = "dataGridViewPlanilla";
            this.dataGridViewPlanilla.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPlanilla.Size = new System.Drawing.Size(878, 299);
            this.dataGridViewPlanilla.TabIndex = 0;
            this.dataGridViewPlanilla.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridViewPlanilla_RowsAdded);
            // 
            // btnObtenerDatosPlanilla
            // 
            this.btnObtenerDatosPlanilla.Location = new System.Drawing.Point(708, 23);
            this.btnObtenerDatosPlanilla.Name = "btnObtenerDatosPlanilla";
            this.btnObtenerDatosPlanilla.Size = new System.Drawing.Size(147, 32);
            this.btnObtenerDatosPlanilla.TabIndex = 2;
            this.btnObtenerDatosPlanilla.Text = "Obtener Datos de Planilla";
            this.btnObtenerDatosPlanilla.UseVisualStyleBackColor = true;
            this.btnObtenerDatosPlanilla.Click += new System.EventHandler(this.btnObtenerDatosPlanilla_Click);
            // 
            // idPlanilla
            // 
            this.idPlanilla.DataPropertyName = "idPlanilla";
            this.idPlanilla.HeaderText = "idPlanilla";
            this.idPlanilla.Name = "idPlanilla";
            this.idPlanilla.Visible = false;
            // 
            // idCalendario
            // 
            this.idCalendario.DataPropertyName = "idCalendario";
            this.idCalendario.HeaderText = "idCalendario";
            this.idCalendario.Name = "idCalendario";
            this.idCalendario.Visible = false;
            // 
            // subtotalPlanilla
            // 
            this.subtotalPlanilla.DataPropertyName = "subtotalPlanilla";
            this.subtotalPlanilla.HeaderText = "subtotalPlanilla";
            this.subtotalPlanilla.Name = "subtotalPlanilla";
            this.subtotalPlanilla.Visible = false;
            // 
            // totalDeducciones
            // 
            this.totalDeducciones.DataPropertyName = "totalDeducciones";
            this.totalDeducciones.HeaderText = "totalDeducciones";
            this.totalDeducciones.Name = "totalDeducciones";
            this.totalDeducciones.Visible = false;
            // 
            // totalPlanilla
            // 
            this.totalPlanilla.DataPropertyName = "totalPlanilla";
            this.totalPlanilla.HeaderText = "totalPlanilla";
            this.totalPlanilla.Name = "totalPlanilla";
            this.totalPlanilla.Visible = false;
            // 
            // idPlanillaDetail
            // 
            this.idPlanillaDetail.DataPropertyName = "idPlanillaDetail";
            this.idPlanillaDetail.HeaderText = "idPlanillaDetail";
            this.idPlanillaDetail.Name = "idPlanillaDetail";
            this.idPlanillaDetail.Visible = false;
            // 
            // idCalendarioDetail
            // 
            this.idCalendarioDetail.DataPropertyName = "idCalendarioDetail";
            this.idCalendarioDetail.HeaderText = "idCalendarioDetail";
            this.idCalendarioDetail.Name = "idCalendarioDetail";
            this.idCalendarioDetail.Visible = false;
            // 
            // nombreCompleto
            // 
            this.nombreCompleto.DataPropertyName = "nombreCompleto";
            this.nombreCompleto.HeaderText = "Nombre de Empleado";
            this.nombreCompleto.Name = "nombreCompleto";
            this.nombreCompleto.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.nombreCompleto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // laborLunes
            // 
            this.laborLunes.DataPropertyName = "codigoLaborLunes";
            this.laborLunes.HeaderText = "Labor Lunes";
            this.laborLunes.Name = "laborLunes";
            this.laborLunes.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // cantLunes
            // 
            this.cantLunes.DataPropertyName = "cantLunes";
            this.cantLunes.HeaderText = "Trabajado Lunes";
            this.cantLunes.Name = "cantLunes";
            this.cantLunes.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cantLunes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // laborMartes
            // 
            this.laborMartes.DataPropertyName = "codigoLaborMartes";
            this.laborMartes.HeaderText = "Labor Martes";
            this.laborMartes.Name = "laborMartes";
            // 
            // cantMartes
            // 
            this.cantMartes.DataPropertyName = "cantMartes";
            this.cantMartes.HeaderText = "Trabajado Martes";
            this.cantMartes.Name = "cantMartes";
            this.cantMartes.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cantMartes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // laborMiercoles
            // 
            this.laborMiercoles.DataPropertyName = "codigoLaborMiercoles";
            this.laborMiercoles.HeaderText = "Labor Miercoles";
            this.laborMiercoles.Name = "laborMiercoles";
            // 
            // cantMiercoles
            // 
            this.cantMiercoles.DataPropertyName = "cantMiercoles";
            this.cantMiercoles.HeaderText = "Trabajado Miercoles";
            this.cantMiercoles.Name = "cantMiercoles";
            // 
            // laborJueves
            // 
            this.laborJueves.DataPropertyName = "codigoLaborJueves";
            this.laborJueves.HeaderText = "Labor Jueves";
            this.laborJueves.Name = "laborJueves";
            // 
            // cantJueves
            // 
            this.cantJueves.DataPropertyName = "cantJueves";
            this.cantJueves.HeaderText = "Trabajado Jueves";
            this.cantJueves.Name = "cantJueves";
            // 
            // laborViernes
            // 
            this.laborViernes.DataPropertyName = "codigoLaborViernes";
            this.laborViernes.HeaderText = "Labor Viernes";
            this.laborViernes.Name = "laborViernes";
            // 
            // cantViernes
            // 
            this.cantViernes.DataPropertyName = "cantViernes";
            this.cantViernes.HeaderText = "Trabajado Viernes";
            this.cantViernes.Name = "cantViernes";
            // 
            // laborSabado
            // 
            this.laborSabado.DataPropertyName = "codigoLaborSabado";
            this.laborSabado.HeaderText = "Labor Sabado";
            this.laborSabado.Name = "laborSabado";
            // 
            // cantSabado
            // 
            this.cantSabado.DataPropertyName = "cantSabado";
            this.cantSabado.HeaderText = "Trabajado Sabado";
            this.cantSabado.Name = "cantSabado";
            // 
            // laborDomingo
            // 
            this.laborDomingo.DataPropertyName = "codigoLaborDomingo";
            this.laborDomingo.HeaderText = "Labor Domingo";
            this.laborDomingo.Name = "laborDomingo";
            // 
            // cantDomingo
            // 
            this.cantDomingo.DataPropertyName = "cantDomingo";
            this.cantDomingo.HeaderText = "Trabajado Domingo";
            this.cantDomingo.Name = "cantDomingo";
            // 
            // subtotal
            // 
            this.subtotal.DataPropertyName = "subtotal";
            this.subtotal.HeaderText = "Subtotal";
            this.subtotal.Name = "subtotal";
            // 
            // deducciones
            // 
            this.deducciones.DataPropertyName = "deduccionesAEmpleado";
            this.deducciones.HeaderText = "Total Deducciones";
            this.deducciones.Name = "deducciones";
            // 
            // total
            // 
            this.total.DataPropertyName = "totalAPagarEmpleado";
            this.total.HeaderText = "Total";
            this.total.Name = "total";
            // 
            // GenerarPlanilla
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 476);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox1);
            this.Name = "GenerarPlanilla";
            this.Text = "Generar Planilla";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlanilla)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxSemanaTrabajo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel labelTarea;
        private System.Windows.Forms.ToolStripProgressBar progreso;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridViewPlanilla;
        private System.Windows.Forms.Button btnObtenerDatosPlanilla;
        private System.Windows.Forms.DataGridViewTextBoxColumn idPlanilla;
        private System.Windows.Forms.DataGridViewTextBoxColumn idCalendario;
        private System.Windows.Forms.DataGridViewTextBoxColumn subtotalPlanilla;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalDeducciones;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalPlanilla;
        private System.Windows.Forms.DataGridViewTextBoxColumn idPlanillaDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn idCalendarioDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreCompleto;
        private System.Windows.Forms.DataGridViewComboBoxColumn laborLunes;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantLunes;
        private System.Windows.Forms.DataGridViewComboBoxColumn laborMartes;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantMartes;
        private System.Windows.Forms.DataGridViewComboBoxColumn laborMiercoles;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantMiercoles;
        private System.Windows.Forms.DataGridViewComboBoxColumn laborJueves;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantJueves;
        private System.Windows.Forms.DataGridViewComboBoxColumn laborViernes;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantViernes;
        private System.Windows.Forms.DataGridViewComboBoxColumn laborSabado;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantSabado;
        private System.Windows.Forms.DataGridViewComboBoxColumn laborDomingo;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantDomingo;
        private System.Windows.Forms.DataGridViewTextBoxColumn subtotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn deducciones;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
    }
}