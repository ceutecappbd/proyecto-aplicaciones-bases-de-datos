﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Proyecto_Aplicaciones_BD.Planillas
{
    public partial class GenerarPlanilla : Form
    {

        MySqlConnection Conexion_MySQL;
        DataTable laboresDT;
        public GenerarPlanilla(FormaPrincipal parent, MySqlConnection con)
        {
            InitializeComponent();
            this.MaximumSize = this.MinimumSize = this.Size;
            this.MdiParent = parent;
            this.Conexion_MySQL = con;

            ObtenerListaDeLabores();
            ObtenerListaDeCalendarios();
        }

        private void ObtenerListaDeCalendarios()
        {
            try
            {
                DataTable dt = new DataTable();
                MySqlDataAdapter adapter = new MySqlDataAdapter("SELECT idCalendario, CONCAT(CAST(trabajoDesde as char),' - ',CAST(trabajoHasta as char)) as semanas FROM calendario", Conexion_MySQL);
                adapter.Fill(dt);
                comboBoxSemanaTrabajo.DataSource = dt;
                comboBoxSemanaTrabajo.ValueMember = "idCalendario";
                comboBoxSemanaTrabajo.DisplayMember = "semanas";
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void comboBoxSemanaTrabajo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ObtenerListaDeLabores()
        {
            try
            {
                laboresDT = new DataTable();
                MySqlDataAdapter adapter = new MySqlDataAdapter("SELECT codigoLabor, CONCAT(nombreLabor, ' - ', unidadMedida) AS descripcionLabor FROM labores", Conexion_MySQL);
                adapter.Fill(laboresDT);
            }
            catch (Exception e)
            {
                MessageBox.Show("Error al actualizar lista de labores. Mensaje : " + e.ToString(), "Error al actualizar", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }


        private void btnObtenerDatosPlanilla_Click(object sender, EventArgs e)
        {
            try
            {
                MySqlCommand comandobuscarplanilla = new MySqlCommand("SELECT idPlanilla FROM planilla WHERE idCalendario=@cal", Conexion_MySQL);
                comandobuscarplanilla.Parameters.AddWithValue("@cal", comboBoxSemanaTrabajo.SelectedValue.ToString());
                Console.WriteLine(comboBoxSemanaTrabajo.SelectedValue);

                if(Convert.ToInt32(comandobuscarplanilla.ExecuteScalar())>0)
                {
                    //Significa que ya hay una planilla
                    MySqlDataReader reader = comandobuscarplanilla.ExecuteReader();
                    int planillaId = -1;
                    while(reader.Read())
                    {
                        planillaId = reader.GetInt32("idPlanilla");
                    }

                    reader.Close();

                    Console.WriteLine("Planilla ID : " + planillaId);
                    MySqlCommand getplanillainfo = new MySqlCommand("SELECT planilla.idPlanilla, planilla.idCalendario, planilla.subtotalPlanilla, planilla.totalDeducciones, planilla.totalPlanilla, planilladetail.idPlanillaDetail, planilladetail.idCalendarioDetail, CONCAT(empleados.nombreEmpleado,' ',empleados.apellidoEmpleado) AS nombreCompleto, calendariodetail.codigoLaborLunes, calendariodetail.codigoLaborMartes, calendariodetail.codigoLaborMiercoles, calendariodetail.codigoLaborJueves, calendariodetail.codigoLaborViernes, calendariodetail.codigoLaborSabado, calendariodetail.codigoLaborDomingo, planilladetail.deduccionesAEmpleado, planilladetail.totalAPagarEmpleado FROM planilla INNER JOIN planilladetail ON planilla.idPlanilla=@planilla INNER JOIN calendariodetail ON planilladetail.idCalendarioDetail=calendariodetail.idCalendarioDetail INNER JOIN empleados ON calendariodetail.idEmpleado=empleados.idEmpleado", Conexion_MySQL);
                    getplanillainfo.Parameters.AddWithValue("@planilla", planillaId);

                    DataTable tablaPlanillaInfo = new DataTable();
                    MySqlDataAdapter adapter = new MySqlDataAdapter(getplanillainfo);
                    adapter.Fill(tablaPlanillaInfo);

                    dataGridViewPlanilla.DataSource = tablaPlanillaInfo;
                }
                else
                {
                    //Significa que tenemos que generar la nueva planilla. Preguntamos si queremos hacer eso.
                    DialogResult resultado = MessageBox.Show("No hay ninguna planilla generada para esta semana. Desea bloquear el calendario y generar una planilla para esta semana?", "Generar Planilla", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if(resultado == DialogResult.Yes)
                    {
                        MySqlCommand crearplanilla = new MySqlCommand("crear_planilla", Conexion_MySQL);
                        crearplanilla.CommandType = CommandType.StoredProcedure;
                        crearplanilla.Parameters.AddWithValue("@idCalendarioInput", comboBoxSemanaTrabajo.SelectedValue.ToString());

                        if(crearplanilla.ExecuteNonQuery()>0)
                        {
                            comandobuscarplanilla = new MySqlCommand("SELECT idPlanilla FROM planilla WHERE idCalendario=@cal", Conexion_MySQL);
                            comandobuscarplanilla.Parameters.AddWithValue("@cal", comboBoxSemanaTrabajo.SelectedValue.ToString());

                            if (Convert.ToInt32(comandobuscarplanilla.ExecuteScalar()) > 0)
                            {
                                //Significa que ya hay una planilla
                                MySqlDataReader reader = comandobuscarplanilla.ExecuteReader();
                                int planillaId = -1;
                                while (reader.Read())
                                {
                                    planillaId = reader.GetInt32("idPlanilla");
                                }

                                reader.Close();

                                Console.WriteLine("Planilla ID : " + planillaId);
                                MySqlCommand getplanillainfo = new MySqlCommand("SELECT planilla.idPlanilla, planilla.idCalendario, planilla.subtotalPlanilla, planilla.totalDeducciones, planilla.totalPlanilla, planilladetail.idPlanillaDetail, planilladetail.idCalendarioDetail, CONCAT(empleados.nombreEmpleado,' ',empleados.apellidoEmpleado) AS nombreCompleto, calendariodetail.codigoLaborLunes, calendariodetail.codigoLaborMartes, calendariodetail.codigoLaborMiercoles, calendariodetail.codigoLaborJueves, calendariodetail.codigoLaborViernes, calendariodetail.codigoLaborSabado, calendariodetail.codigoLaborDomingo, planilladetail.deduccionesAEmpleado, planilladetail.totalAPagarEmpleado FROM planilla INNER JOIN planilladetail ON planilla.idPlanilla=@planilla INNER JOIN calendariodetail ON planilladetail.idCalendarioDetail=calendariodetail.idCalendarioDetail INNER JOIN empleados ON calendariodetail.idEmpleado=empleados.idEmpleado", Conexion_MySQL);
                                getplanillainfo.Parameters.AddWithValue("@planilla", planillaId);

                                DataTable tablaPlanillaInfo = new DataTable();
                                MySqlDataAdapter adapter = new MySqlDataAdapter(getplanillainfo);
                                adapter.Fill(tablaPlanillaInfo);

                                dataGridViewPlanilla.DataSource = tablaPlanillaInfo;
                            }
                        }
                    }
                }

            }
            catch(Exception ex)
            {
                Console.Write(ex.ToString());
            }
        }

        private void dataGridViewPlanilla_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            foreach (DataGridViewCell cell in dataGridViewPlanilla.Rows[e.RowIndex].Cells)
            {
                if (cell is DataGridViewComboBoxCell)
                {
                    ((DataGridViewComboBoxCell)cell).DataSource = laboresDT;
                    ((DataGridViewComboBoxCell)cell).DisplayMember = "descripcionLabor";
                    ((DataGridViewComboBoxCell)cell).ValueMember = "codigoLabor";
                    ((DataGridViewComboBoxCell)cell).ReadOnly = true;
                    Console.WriteLine("Updating combobox displaymember");
                }
            }
        }

    }
}
