﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proyecto_Aplicaciones_BD.Entidades_BD
{
    public class Admin
    {

        private int idusuario;
        private String usuario;
        private String nombre;
        private String apellido;
        private String correo;

        public int IDUsuario
        {
            get
            {
                return idusuario;
            }
            set
            {
                idusuario = value;
            }
        }

        public String Usuario
        {
            get
            {
                return usuario;
            }
            set
            {
                usuario = value;
            }
        }

        public String Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }

        public String Apellido
        {
            get
            {
                return apellido;
            }
            set
            {
                apellido = value;
            }
        }

        public String Correo
        {
            get
            {
                return correo;
            }
            set
            {
                correo = value;
            }
        }

    }
}
