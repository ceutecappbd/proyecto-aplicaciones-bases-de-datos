﻿namespace Proyecto_Aplicaciones_BD
{
    partial class FormaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.mysqlstatusstrip = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seguridadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarSesionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionarEmpleadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionarLaboresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionarCalendarioDeLaboresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionarDeduccionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.planillasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarPlanillaDePagosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportePlanillaSemanalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reporteLaboresPorSemanaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mysqlstatusstrip});
            this.statusStrip.Location = new System.Drawing.Point(0, 415);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(789, 22);
            this.statusStrip.TabIndex = 1;
            this.statusStrip.Text = "statusStrip";
            // 
            // mysqlstatusstrip
            // 
            this.mysqlstatusstrip.Name = "mysqlstatusstrip";
            this.mysqlstatusstrip.Size = new System.Drawing.Size(162, 17);
            this.mysqlstatusstrip.Text = "Usuario conectado : Ninguno";
            // 
            // menuStrip
            // 
            this.menuStrip.Enabled = false;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.gestionesToolStripMenuItem,
            this.planillasToolStripMenuItem,
            this.reportesToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(789, 24);
            this.menuStrip.TabIndex = 2;
            this.menuStrip.Text = "menuStrip";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.seguridadToolStripMenuItem,
            this.cerrarSesionToolStripMenuItem,
            this.cerrarToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // seguridadToolStripMenuItem
            // 
            this.seguridadToolStripMenuItem.Name = "seguridadToolStripMenuItem";
            this.seguridadToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.seguridadToolStripMenuItem.Text = "Administrar Seguridad";
            // 
            // cerrarSesionToolStripMenuItem
            // 
            this.cerrarSesionToolStripMenuItem.Name = "cerrarSesionToolStripMenuItem";
            this.cerrarSesionToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F11;
            this.cerrarSesionToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.cerrarSesionToolStripMenuItem.Text = "Cerrar Sesion";
            this.cerrarSesionToolStripMenuItem.Click += new System.EventHandler(this.cerrarSesionToolStripMenuItem_Click);
            // 
            // cerrarToolStripMenuItem
            // 
            this.cerrarToolStripMenuItem.Name = "cerrarToolStripMenuItem";
            this.cerrarToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.cerrarToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.cerrarToolStripMenuItem.Text = "Cerrar";
            this.cerrarToolStripMenuItem.Click += new System.EventHandler(this.cerrarToolStripMenuItem_Click);
            // 
            // gestionesToolStripMenuItem
            // 
            this.gestionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionarEmpleadosToolStripMenuItem,
            this.gestionarLaboresToolStripMenuItem,
            this.gestionarCalendarioDeLaboresToolStripMenuItem,
            this.gestionarDeduccionesToolStripMenuItem});
            this.gestionesToolStripMenuItem.Name = "gestionesToolStripMenuItem";
            this.gestionesToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.gestionesToolStripMenuItem.Text = "Administrar";
            // 
            // gestionarEmpleadosToolStripMenuItem
            // 
            this.gestionarEmpleadosToolStripMenuItem.Name = "gestionarEmpleadosToolStripMenuItem";
            this.gestionarEmpleadosToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.gestionarEmpleadosToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.gestionarEmpleadosToolStripMenuItem.Text = "Administrar Empleados";
            this.gestionarEmpleadosToolStripMenuItem.Click += new System.EventHandler(this.gestionarEmpleadosToolStripMenuItem_Click);
            // 
            // gestionarLaboresToolStripMenuItem
            // 
            this.gestionarLaboresToolStripMenuItem.Name = "gestionarLaboresToolStripMenuItem";
            this.gestionarLaboresToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.gestionarLaboresToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.gestionarLaboresToolStripMenuItem.Text = "Administrar Labores";
            this.gestionarLaboresToolStripMenuItem.Click += new System.EventHandler(this.gestionarLaboresToolStripMenuItem_Click);
            // 
            // gestionarCalendarioDeLaboresToolStripMenuItem
            // 
            this.gestionarCalendarioDeLaboresToolStripMenuItem.Name = "gestionarCalendarioDeLaboresToolStripMenuItem";
            this.gestionarCalendarioDeLaboresToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.gestionarCalendarioDeLaboresToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.gestionarCalendarioDeLaboresToolStripMenuItem.Text = "Administrar Calendario de Labores";
            this.gestionarCalendarioDeLaboresToolStripMenuItem.Click += new System.EventHandler(this.gestionarCalendarioDeLaboresToolStripMenuItem_Click);
            // 
            // gestionarDeduccionesToolStripMenuItem
            // 
            this.gestionarDeduccionesToolStripMenuItem.Name = "gestionarDeduccionesToolStripMenuItem";
            this.gestionarDeduccionesToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.gestionarDeduccionesToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.gestionarDeduccionesToolStripMenuItem.Text = "Administrar Deducciones";
            this.gestionarDeduccionesToolStripMenuItem.Click += new System.EventHandler(this.gestionarDeduccionesToolStripMenuItem_Click);
            // 
            // planillasToolStripMenuItem
            // 
            this.planillasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generarPlanillaDePagosToolStripMenuItem});
            this.planillasToolStripMenuItem.Name = "planillasToolStripMenuItem";
            this.planillasToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.planillasToolStripMenuItem.Text = "Planillas";
            // 
            // generarPlanillaDePagosToolStripMenuItem
            // 
            this.generarPlanillaDePagosToolStripMenuItem.Name = "generarPlanillaDePagosToolStripMenuItem";
            this.generarPlanillaDePagosToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.generarPlanillaDePagosToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.generarPlanillaDePagosToolStripMenuItem.Text = "Generar Planilla de Pagos";
            this.generarPlanillaDePagosToolStripMenuItem.Click += new System.EventHandler(this.generarPlanillaDePagosToolStripMenuItem_Click);
            // 
            // reportesToolStripMenuItem
            // 
            this.reportesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reportePlanillaSemanalToolStripMenuItem,
            this.reporteLaboresPorSemanaToolStripMenuItem});
            this.reportesToolStripMenuItem.Name = "reportesToolStripMenuItem";
            this.reportesToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.reportesToolStripMenuItem.Text = "Reportes";
            // 
            // reportePlanillaSemanalToolStripMenuItem
            // 
            this.reportePlanillaSemanalToolStripMenuItem.Name = "reportePlanillaSemanalToolStripMenuItem";
            this.reportePlanillaSemanalToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.S)));
            this.reportePlanillaSemanalToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.reportePlanillaSemanalToolStripMenuItem.Text = "Reporte Planilla Semanal";
            this.reportePlanillaSemanalToolStripMenuItem.Click += new System.EventHandler(this.reportePlanillaSemanalToolStripMenuItem_Click);
            // 
            // reporteLaboresPorSemanaToolStripMenuItem
            // 
            this.reporteLaboresPorSemanaToolStripMenuItem.Name = "reporteLaboresPorSemanaToolStripMenuItem";
            this.reporteLaboresPorSemanaToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.P)));
            this.reporteLaboresPorSemanaToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.reporteLaboresPorSemanaToolStripMenuItem.Text = "Reporte Labores por Semana ";
            this.reporteLaboresPorSemanaToolStripMenuItem.Click += new System.EventHandler(this.reporteLaboresPorSemanaToolStripMenuItem_Click);
            // 
            // FormaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto_Aplicaciones_BD.Properties.Resources.Mimetypes_x_office_spreadsheet_icon;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(789, 437);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.DoubleBuffered = true;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "FormaPrincipal";
            this.Text = "Proyecto Aplicaciones de Base de Datos - CEUTEC";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormaPrincipal_Load);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionarLaboresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionarEmpleadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionarCalendarioDeLaboresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionarDeduccionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem planillasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generarPlanillaDePagosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportePlanillaSemanalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reporteLaboresPorSemanaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seguridadToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel mysqlstatusstrip;
        private System.Windows.Forms.ToolStripMenuItem cerrarSesionToolStripMenuItem;
    }
}

