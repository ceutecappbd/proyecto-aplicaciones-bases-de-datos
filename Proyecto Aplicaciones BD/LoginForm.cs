﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Proyecto_Aplicaciones_BD.Entidades_BD;

namespace Proyecto_Aplicaciones_BD
{
    public partial class LoginForm : Form
    {

        private MySqlConnection Conexion_MySQL;

        public LoginForm(FormaPrincipal MDIParent, MySqlConnection con)
        {
            InitializeComponent();
            //Toda forma dentro de la forma MDI tiene que setear su MDIParent como la FormaPrincipal
            this.MdiParent = MDIParent;
            this.Conexion_MySQL = con;
            this.MaximumSize = this.MinimumSize = this.Size;
            this.CenterToParent();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            String usuario = userTextBox.Text;
            String password = passwordTextBox.Text;

            if(ProcesarLogin(usuario, password))
            {
                ((FormaPrincipal)this.MdiParent).LoginSuccesful();
            }
            else
            {
                MessageBox.Show("Usuario/Password invalido.", "Error de Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool ProcesarLogin(String usuario, String password)
        {
            //Sacamos el Hash SHA256 del string de password y lo vamos a comparar con el que 
            //esta en la base de datos.

            byte[] data = System.Text.Encoding.UTF8.GetBytes(password);
            data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
            StringBuilder sb = new StringBuilder();

            //Agregar x2 para formato Hexadecimal requerido para obtener el hash 'legible'
            foreach (byte b in data)
                sb.Append(b.ToString("x2"));

            String hash = sb.ToString();

            //Preparamos el Query para ejecutarlo con el usuario y hash
            MySqlCommand query = new MySqlCommand("SELECT idadmin, nombre, apellido, correo FROM admins WHERE usuario = @username and password = @pass ", Conexion_MySQL);
            //Parametrizar los querys de MySQL para evitar inyecciones de codigo en los parametros enviados.
            //Muy utilizado para elevar la seguridad de la app.
            query.Parameters.AddWithValue("@username", usuario);
            query.Parameters.AddWithValue("@pass", hash);

            Console.WriteLine(hash);

            //Procurar que todo query o acceso a MySQL este dentro de un try/catch para minimizar los crash de
            //la aplicacion.

            try
            {
                MySqlDataReader datareader = query.ExecuteReader();

                if (datareader.Read())
                {
                    //Si el DataReader puede leer 1 vez significa que si encontramos un registro valido 
                    //del usuario. Entonces procedemos a conseguir la info de ese usuario segun lo 
                    //haya leido el DataReader.
                    Admin admin = new Admin();

                    admin.IDUsuario = int.Parse(datareader["idadmin"].ToString());
                    admin.Usuario = usuario;
                    admin.Nombre = datareader["nombre"].ToString();
                    admin.Apellido = datareader["apellido"].ToString();
                    admin.Correo = datareader["correo"].ToString();

                    ((FormaPrincipal)this.MdiParent).Administrador = admin;

                    //Siempre cerrar DataReader
                    datareader.Close();
                    return true;
                }
                else
                {
                    //No encontramos el usuario en la base de datos
                    passwordTextBox.Clear();
                    //Siempre cerrar DataReader
                    datareader.Close();
                    return false;
                }

            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message, "Excepcion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
